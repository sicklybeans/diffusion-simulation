import sys
import os, json
import numpy as np
import seaborn
import matplotlib.pyplot as plt
import sim
import progressbar
from sim import Output

MAX_PLOT = 10

def expected_ca_t_values(times, peq, k_on, k_off):
  c0 = peq - peq * peq
  om = k_on + k_off
  return c0 * np.exp(-times*om)

def go(direc, fnames, use_kon=True, include_predicted_fits=True):
  fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(6.0, 18.0))
  ax1.set_xlabel('t')
  ax1.set_ylabel(r'$C(t)$')
  ax2.set_xlabel('t')
  ax2.set_ylabel(r'$\log(C(t))$')
  # ax3.set_xlabel(r'$k_{off}$')
  # ax3.set_ylabel(r'$\omega$')
  fig.suptitle(r'$C(t) = \langle(a-\langle a\rangle)^2\rangle$')

  koffs = []
  kons = []
  exponents = []

  TIME_CUT = 1.0
  ax1.set_xlim(0, TIME_CUT)
  ax2.set_xlim(0, TIME_CUT)

  if len(fnames) > MAX_PLOT:
    skip = int(len(fnames)/MAX_PLOT)
  else:
    skip = 1

  for i, fname in enumerate(progressbar.progressbar(fnames)):
    output = Output.load('%s/%s' % (direc, fname))
    params = output.params
    states0 = output.states[0,:]
    peq = params.compute_peq()
    k_on = params.k_on
    k_off = params.k_off
    c0 = peq - peq*peq

    # stop_index = None
    # for step, time in enumerate(params.times):
    #   if time > TIME_CUT:
    #     stop_index = step
    #     break

    times = params.times
    num_times = len(params.times)

    ca_t = np.zeros(num_times)

    for step, time in enumerate(times):
      ca_t[step] = np.average(states0[:] * output.states[step, :] - peq*peq)

    log_ca_t = np.log(np.abs(ca_t[:]))

    (m, b) = np.polyfit(times, log_ca_t, 1)
    exponents.append(m)
    koffs.append(k_off)
    kons.append(k_on)

    if i % skip == 0:
      label = '$k_{on}=%.2f$, $k_{off}=%.2f$, $p_{eq}=%.2f$' % (params.k_on, params.k_off, peq)
      clr = ax1.plot(times, ca_t, label=label)[0].get_color()

      ax1.plot([0.0], [peq - peq*peq], color=clr, marker='.')
      ax2.plot(times, log_ca_t, color=clr)
      ax2.plot(times, m * times + b, color=clr, linestyle='dashed', linewidth=0.5)

      if include_predicted_fits:
        predicted = expected_ca_t_values(times, peq, k_on, k_off)
        ax1.plot(times, predicted, color=clr, linestyle='dashed')

  # Fit the value of omega / kon
  k_vals = use_kon and kons or koffs
  (a1, a0) = np.polyfit(k_vals, exponents, 1)
  ax3.plot(k_vals, exponents, label='m=%.2f' % a1)
  ax3.plot(k_vals, [a0 + a1*k for k in k_vals], linestyle='dashed', linewidth=1.0)
  if use_kon:
    ax3.set_xlabel(r'$k_{on}$')
  else:
    ax3.set_xlabel(r'$k_{off}$')
  ax3.set_ylabel(r'$\omega$')

  ax1.legend()
  ax3.legend()
  fig.savefig('%s/fig.png' % direc)
  plt.show()

def analyze():
  direc = 'test10'
  vals = np.linspace(0, 2, 51)[1:]
  fnames =['run_%.2f' % k for k in vals]

  def go_k_on():
    print('Analyzing varied k_on data')
    go('%s/k_on' % direc, fnames, use_kon=True)

  def go_k_off():
    print('Analyzing varied k_off data')
    go('%s/k_off' % direc, fnames, use_kon=False)

  def go_k_both():
    print('Analyzing varied k_both data')
    go('%s/k_both' % direc, fnames, use_kon=True)

  go_k_off()

if __name__ == '__main__':
  analyze()
