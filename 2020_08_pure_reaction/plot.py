import sys
import os, json
import numpy as np
import seaborn
import matplotlib.pyplot as plt
import sim
from sim import Output


def state_fraction_plot(output: Output, ax) -> None:
  peq = output.params.compute_peq()
  ax.plot(output.params.times, output.fraction)
  ax.plot([0.0, output.params.t_end], [peq, peq])
  ax.set_ylim(0, 1.0)
  ax.set_xlabel('Time')
  ax.set_ylabel('Activity')

def density_histogram_plot(output: Output, ax) -> None:
  nt = 20
  nx = 20

  tend = output.params.t_end
  size = output.params.size

  twidth = tend / nt
  xwidth = size / nx

  tbins = np.array([tend * i/nt for i in range(1, nt + 1)])
  xbins = np.array([size * i/nx for i in range(1, nx + 1)])
  hist = np.zeros((nt, nx), np.int)

  for step, positions in enumerate(output.positions):
    tbin = int(output.params.times[step] / twidth)
    for x in positions:
      xbin = int(x / xwidth)
      hist[tbin, xbin] += 1
  seaborn.heatmap(hist, ax=ax)

def do_all(fname: str) -> None:
  fig, (ax1, ax2) = plt.subplots(1, 2)
  output = Output.load(fname)
  state_fraction_plot(output, ax1)
  density_histogram_plot(output, ax2)
  plt.show()

if __name__ == '__main__':
  do_all(sys.argv[1])
