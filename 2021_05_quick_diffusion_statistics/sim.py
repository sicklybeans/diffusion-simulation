import h5py
import numpy as np
import matplotlib.pyplot as plt
import progressbar
import argparse

def run(filename, N, dt, t_end, a0, alpha, verbose=True):
  ka = alpha * a0
  kb = alpha - ka

  prob_a = ka * dt
  prob_b = kb * dt

  n_steps = int(np.ceil(t_end / dt))
  time = np.array([dt * i for i in range(n_steps)])
  activity = np.zeros((n_steps, N))
  states = True + np.zeros((N), dtype=np.bool_)

  bar = verbose and progressbar.ProgressBar() or progressbar.NullBar()

  for i in bar(range(n_steps)):
    activity[i,:] = states[:]

    rands = np.random.random(N)
    inactive = ~states
    remain_in_a = rands[  states] >= prob_b
    switch_to_a = rands[inactive] < prob_a

    states[ states ] = remain_in_a
    states[inactive] = switch_to_a

  with h5py.File(filename, "w") as f:
    f.attrs['N'] = N
    f.attrs['t_end'] = t_end
    f.attrs['dt'] = dt
    f.attrs['alpha'] = alpha
    f.attrs['a0'] = a0
    f.attrs['ka'] = ka
    f.attrs['kb'] = kb

    time_ds = f.create_dataset("time", (n_steps,), dtype='f')
    time_ds[:] = time[:]
    activity_ds = f.create_dataset("activity", (n_steps,N), dtype='b')
    activity_ds[:, :] = activity[:, :]

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Simulation two-state reaction')
  parser.add_argument('output', type=str, help='Output file name')
  parser.add_argument('number', type=int, help='Number of particles in simulation')
  parser.add_argument('average', type=float, help='Average activity level')
  parser.add_argument('alpha', type=float, help='Speed of reaction (ka + kb)')
  parser.add_argument('--dt', type=float, help='Time increment', default=0.01)
  parser.add_argument('--tend', type=float, help='Stopping time', default=10.0)
  parser.add_argument('--verbose', type=bool, help='Verbose mode', default=True)
  args = parser.parse_args()

  run(args.output, args.number, args.dt, args.tend, args.average, args.alpha, args.verbose)
