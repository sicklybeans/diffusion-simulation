import h5py
import numpy as np
import matplotlib.pyplot as plt
import progressbar
import argparse
import sys

FIG_SIZE = (8, 8)

def predicted_activity(time, a0, alpha):
  return (1 - a0) * np.exp(- alpha * time) + a0

def make_plot(input_files):

  fig, ax = plt.subplots(1, 1, figsize=FIG_SIZE)

  for i, filename in enumerate(input_files):
    f = h5py.File(filename, 'r')
    num = f.attrs['N']
    alpha = f.attrs['alpha']
    a0 = f.attrs['a0']

    time = np.array(f['time'])
    activity = np.array(f['activity'])
    averages = np.sum(activity, axis=1) / num
    predicted = predicted_activity(time, a0, alpha)

    ln, = ax.plot(time, averages, label=r'$\bar{a}=%.1f, \alpha=%.2f$' % (a0, alpha))
    ax.plot(time, predicted, ':', color=ln.get_color())
  ax.plot([], [], ':', color='black', label='predicted values')

  ax.legend()
  ax.set_xlabel('Time (s)', fontsize=16)
  ax.set_ylabel(r'$\langle a(t)\rangle$', fontsize=16)
  ax.set_title(r'Proof that $p_a(1,t|1,0)=(1-\langle a\rangle)e^{-\alpha t} + \langle a\rangle$', fontsize=20)

  fig.savefig('fig.png')
  plt.show()


if __name__ == '__main__':
  plt.rcParams.update({'text.usetex': True})
  parser = argparse.ArgumentParser(description='Plot p(1,t|1,0)')
  parser.add_argument('input_files', type=str, nargs='+', help='Input files (glob or space separated)')
  args = parser.parse_args()
  make_plot(args.input_files)
