import progressbar
import numpy as np
import environ

def sim(jobname, num_particles, num_trials, half_size, D, dt, end_time):
  environ.get_simulation_dir(jobname, create_if_missing=True)

  num_times = int(np.ceil((end_time / dt)))

  size = 2 * half_size
  sqrt2Dt = np.sqrt(2 * D * dt)

  for j in progressbar.progressbar(range(num_trials)):
    out_positions = np.zeros((num_times, num_particles, 2))
    out_times = np.zeros(num_times)
    position = (2 * half_size) * np.random.random((num_particles, 2)) - half_size

    for i in range(num_times):
      out_times[i] = dt * i
      out_positions[i] = position

      dposition = sqrt2Dt * np.random.normal(size=(num_particles, 2))
      position[:,:] = (position[:,:] + dposition[:,:])
      too_small = position[:,:] < half_size
      position[too_small] += size
      too_large = position[:,:] >= half_size
      position[too_large] -= size

    file = environ.get_simulation_file(jobname, 'positions', j)
    np.savez(file, positions=out_positions, times=out_times, D=D, dt=dt, end_time=end_time, num_particles=num_particles, jobname=jobname, half_size=half_size)

if __name__ == '__main__':
  setname = 'test_Dt_constant'
  num_p = 1000
  num_t = 500
  half_size = 50
  dt = 0.05
  num_steps = [10, 32, 100, 316, 1000, 3162, 10000]
  Dt = 0.3
  for n_steps in num_steps:
    t_end = n_steps * dt
    D = Dt / t_end
    sim('%s/test_%d' % (setname, n_steps), num_p, num_t, half_size, D, dt, t_end)
