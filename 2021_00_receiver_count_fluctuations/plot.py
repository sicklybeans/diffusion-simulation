import progressbar
import numpy as np
import environ
import os
import sys
import matplotlib.pyplot as plt

def plot(jobname):
  f = environ.get_simulation_file(jobname, 'count_correlations')
  d = np.load(f)


  for i, r in enumerate(d['radiuses']):
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    x_data = d['times']
    y_data = d['correlation'][i]
    ax.plot(x_data, y_data)
    ax.set_xlim(0, max(d['times']))
    ax.set_ylim(-5, 15)
    ax.set_title(r'$\langle \delta Y(t)\delta Y(0)\rangle$ with $r=%.2f$' % r)
    fig.savefig(environ.get_plot_file(jobname, 'fixed_radius_r%.2f' % r))
    fig.clf()


if __name__ == '__main__':
  plot('test_D=E-1')
