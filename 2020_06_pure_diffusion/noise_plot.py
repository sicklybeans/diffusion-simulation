import sys
import os, json
import numpy as np
import environ
import matplotlib.pyplot as plt
from density import DensityField
from sim_runner import Run
from parameters import Parameters
from util import print_progress_bar

if len(sys.argv) != 3:
  print('Usage: %s <job name> <calc name>' % sys.argv[0])
  sys.exit(1)

jobname = sys.argv[1]
calcname = sys.argv[2]

ONE_D = True


params = Parameters.load(jobname)
data = np.load(environ.get_noise_file(jobname, calcname))
plot_file = environ.get_noise_plot(jobname, calcname, '1d-scans.png')

cov_xt = data['covariance_xt']
points = data['points']
times = data['times']
sigma2 = data['sigma2']
x0_ind = data['x0']
t0_ind = data['t0']
dx = points[1] - points[0]
dt = times[1] - times[0]

def compute_traversal_fraction(params: Parameters) -> float:
  x2_avg = 2 * params.D * params.t_end
  t_frac = np.sqrt(x2_avg) / params.size
  return t_frac

print('Sigma: %.2f' % sigma2)
print('Traversal fraction: %.2f' % compute_traversal_fraction(params))

if ONE_D:
  fig, (ax1, ax2) = plt.subplots(2, 1)
  x_values = [dx*i for i in range(len(cov_xt))]
  t_values = [dt*i for i in range(len(cov_xt[0]))]
  print(max(t_values))
  ax1.plot(x_values, cov_xt[:,0])
  ax1.set_xlabel(r'$x$')
  ax1.set_ylabel(r'$C(x,t_0)$')
  ax2.plot(t_values, cov_xt[0,:])
  ax2.set_xlabel(r'$t$')
  ax2.set_ylabel(r'$C(x_0,t)$')

  fig.savefig(plot_file)
  plt.show()

else:
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  x_values = []
  t_values = []
  s_values = []
  for xi in range(len(cov_xt)):
    for ti in range(len(cov_xt[0])):
      x_values.append(xi * dx)
      t_values.append(ti * dt)
      s_values.append(cov_xt[xi,ti])

  ax.scatter(x_values, t_values, s_values)
  plt.show()
