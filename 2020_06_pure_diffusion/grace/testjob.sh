#!/bin/bash
#SBATCH --partition="scavenge"
#SBATCH --job-name="diff test1"
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=8000
#SBATCH --time='12:00:00'
#SBATCH --mail-type=FAIL


JOBNAME="test1"
NUM_PARTICLES=10000
SIZE=10.0
T_END=5.0
DT=0.025
D=1

START=0
END=100

export PYTHONPATH=/home/fas/machta/sjb94/diffusion-simulation


module load Python/3.7.0-foss-2018b

python3.7 -m "grace.simulator" "$JOBNAME" "$NUM_PARTICLES" "$SIZE" "$DT" "$T_END" "$D" "$START" "$END"
