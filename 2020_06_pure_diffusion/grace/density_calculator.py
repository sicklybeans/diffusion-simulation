import argparse
import numpy as np
import environ
import sim
from parameters import Parameters

def go(jobname: str, calcname: str, start_index: int, end_index: int) -> None:
  params = Parameters.load(jobname)
  density_params = DensityParams.load(jobname, calcname)

  for i in range(start_index, end_index):
    data = np.load(environ.get_position_file(self.jobname, i))
    positions = data['positions']
    times = data['times']
    dfield = DensityField.generate(params, positions, density_params.num_points, density_params.sigma2, times, verbose=False)
    dfield.save(jobname, calcname, i)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('jobname', type=str, help='Name of job')
  parser.add_argument('calcname', type=str, help='Name of calculation group')
  parser.add_argument('st_index', type=float, help='Index of first trial')
  parser.add_argument('ed_index', type=float, help='Index of last trial')
  args = parser.parse_args(sys.argv[1:])
  go(args.jobname, args.st_index, args.ed_index)
