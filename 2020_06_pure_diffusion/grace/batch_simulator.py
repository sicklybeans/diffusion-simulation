import argparse
import sys
import environ
import numpy as np
from parameters import Parameters

JOBNAME = 'diff test1'
PARTITION = 'scavenge'
RAM = '8000'
TIME = '12:00:00'

def go(jobname: str, num_particles: int, size: float, dt: float, t_end: float, d: float, trials_per_job: int, num_trials: int) -> None:
  params = Parameters(1, size, num_particles, d, dt, t_end, num_trials)
  params.save(jobname)

  num_batches = int(np.ceil(num_trials / trials_per_job))

  for i in range(num_batches):
    job_file = environ.get_sim_job_file(jobname, i)

    f = open(job_file, 'w')
    f.write('#!/bin/bash\n')
    f.write('#SBATCH --partition="%s"\n' % PARTITION)
    f.write('#SBATCH --job-name="%s"\n' % JOBNAME)
    f.write('#SBATCH --ntasks=1 --nodes=1\n')
    f.write('#SBATCH --mem-per-cpu=%s\n' % RAM)
    f.write('#SBATCH --time="%s"\n' % TIME)
    f.write('export PYTHONPATH=/home/fas/machta/sjb94/diffusion-simulation\n')
    f.write('module load Python/3.7.0-foss-2018b\n')
    start = i * trials_per_job
    end = min(start + trials_per_job, num_trials)
    f.write('python3.7 -m "grace.simulator" "%s" "%d" "%d"\n' % (jobname, start, end))

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('jobname', type=str, help='Name of job')
  parser.add_argument('num_particles', type=int, help='Number of particles')
  parser.add_argument('size', type=float, help='Width of box')
  parser.add_argument('dt', type=float, help='Simulation timestep')
  parser.add_argument('t_end', type=float, help='End time of simulation')
  parser.add_argument('d', type=float, help='Diffusion constant')
  parser.add_argument('trials_per_job', type=int, help='Number of trials in each job')
  parser.add_argument('num_trials', type=int, help='Number of trials')
  args = parser.parse_args(sys.argv[1:])
  go(args.jobname, args.num_particles, args.size, args.dt, args.t_end, args.d, args.trials_per_job, args.num_trials)
