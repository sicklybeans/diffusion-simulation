from __future__ import annotations
import numpy as np
import environ
import json
from typing import Dict, Any

class TypeException(Exception):

  def __init__(self, name: str, expected_type: str) -> None:
    Exception.__init__(self, 'Expected %s to be a %s' % (name, expected_type))

class Parameters(object):
  dims: int
  size: float
  n_particles: int
  D: float
  dt: float
  t_end: float
  n_steps: int
  sqrt2Ddt: float
  density: float
  num_trials: int

  def __init__(self,
      dims: int,
      size: float,
      n_particles: int,
      D: float,
      dt: float,
      t_end: float,
      num_trials: int) -> None:
    self.dims = dims
    self.size = size
    self.n_particles = n_particles
    self.D = D
    self.dt = dt
    self.t_end = t_end
    self.n_steps = int(np.ceil(self.t_end / self.dt) + 1)
    self.sqrt2Ddt = np.sqrt(2*self.D*self.dt)
    self.density = self.n_particles / (self.size ** self.dims)
    self.num_trials = num_trials
    if not isinstance(self.size, float):
      raise TypeException('size', 'float')
    if not isinstance(self.t_end, float):
      raise TypeException('t_end', 'float')

  def __repr__(self) -> str:
    return '  dims: %d\n  size: %.1f\n  n_parts: %d\n  D: %.2f\n  dt: %.2f\n  n_steps: %d' % (self.dims, self.size, self.n_particles, self.D, self.dt, self.n_steps)

  def _to_json(self) -> Dict[str, Any]:
    return {
      'dims': self.dims,
      'size': self.size,
      'n_particles': self.n_particles,
      'D': self.D,
      'dt': self.dt,
      't_end': self.t_end,
      'density': self.density,
      'n_steps': self.n_steps,
      'num_trials': self.num_trials
    }

  @classmethod
  def _from_json(cls, json_dict: Dict[str, Any]) -> Parameters:
    return cls(
      json_dict['dims'],
      json_dict['size'],
      json_dict['n_particles'],
      json_dict['D'],
      json_dict['dt'],
      json_dict['t_end'],
      json_dict['num_trials'])

  @classmethod
  def load(cls, jobname: str) -> Parameters:
    param_file = environ.get_param_file(jobname)
    return cls._from_json(json.loads(open(param_file, 'r').read()))

  def save(self, jobname: str) -> None:
    param_file = environ.get_param_file(jobname)
    with open(param_file, 'w') as f:
      json.dump(self._to_json(), f, indent=2)

class Output(object):
  params: Parameters
  positions: np.ndarray
  times: np.ndarray

  def __init__(self, params: Parameters) -> None:
    self.params = params
    self.positions = np.zeros((params.n_steps, params.n_particles, params.dims))
    self.times = np.zeros((params.n_steps))

  def write(self, current_positions: np.ndarray, n_step: int) -> None:
    self.positions[n_step,:,:] = current_positions[:,:]
    self.times[n_step] = n_step * self.params.dt

  @classmethod
  def load(cls, jobname: str, trial_index: int) -> Output:
    params = Parameters.load(jobname)
    data = np.load(environ.get_position_file(jobname, trial_index))
    out = Output(params)
    out.positions = data['positions']
    out.times = data['times']
    return out

  def save(self, jobname: str, trial_index: int) -> None:
    out_file = environ.get_position_file(jobname, trial_index)
    np.savez(out_file, positions=self.positions, times=self.times)
