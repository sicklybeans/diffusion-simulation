import argparse
import sys
import os, json
import numpy as np
import environ
import threading
import time
from density import DensityField
from sim_runner import Run
from parameters import Parameters
from util import print_progress_bar

class Analysis(threading.Thread):
  jobname: str
  calcname: str
  params: Parameters
  n_points: int
  sigma2: float
  start_trial: int
  end_trial: int
  verbose: bool

  num_completed = 0
  num_completed_lock = threading.Lock()

  def __init__(self, jobname: str, calcname: str, params: Parameters, start_trial: int, end_trial: int, verbose: bool=False) -> None:
    threading.Thread.__init__(self)
    self.jobname = jobname
    self.calcname = calcname
    self.params = params
    self.n_points = n_points
    self.sigma2 = sigma2
    self.start_trial = start_trial
    self.end_trial = end_trial
    self.verbose = verbose

  def run(self) -> None:
    for i in range(self.start_trial, self.end_trial):
      if self.verbose:
        print_progress_bar(i, self.end_trial - self.start_trial)

      data = np.load(environ.get_position_file(self.jobname, i))
      positions = data['positions']
      times = data['times']
      dfield = DensityField.generate(self.params, positions, self.n_points, self.sigma2, times, verbose=False)
      dfield.save(self.jobname, self.calcname, i)

      DensityThread.num_completed_lock.acquire()
      DensityThread.num_completed += 1
      print_progress_bar(DensityThread.num_completed, self.params.num_trials)
      DensityThread.num_completed_lock.release()

    if self.verbose:
      print_progress_bar(self.end_trial, self.end_trial)

def go(jobname: str, calcname: str, sigma2: float, num_threads: int=12) -> None:
  params = Parameters.load(jobname)
  t0 = time.time()

  Analysis(jobname, calcname, params, 0, params.num_trials, verbose=True).run()
  t1 = time.time()
  print('Finished in %.2f seconds' % (t1-t0))


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument(
    'jobname',
    type=str,
    help='Name of job')
  parser.add_argument(
    'calcname',
    type=str,
    help='Name for this density calculation')
  parser.add_argument(
    'radius',
    type=float,
    help='Radius of receiver')
  parser.add_argument(
    '-t', '--threads',
    type=int,
    help='Number of threads to use (default=12)',
    default=12)
  args = parser.parse_args(sys.argv[1:])
  go(args.jobname, args.calcname, args.radius, num_threads=args.threads)




