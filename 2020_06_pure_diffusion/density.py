from __future__ import annotations
import numpy as np
import environ
from parameters import Parameters
from util import print_progress_bar

class DensityParams(object):
  sigma2: float
  num_points: int

  def __init__(self, sigma2: float, num_points: int) -> None:
    self.sigma2 = sigma2
    self.num_points = num_points

  def _to_json(self) -> Dict[str, Any]:
    return {
      'sigma2': self.sigma2,
      'num_points': self.num_points
    }

  @classmethod
  def _from_json(cls, json_dict: Dict[str, Any]) -> Parameters:
    return cls(
      json_dict['sigma2'],
      json_dict['num_points'])

  @classmethod
  def load(cls, jobname: str, calcname: str) -> Parameters:
    param_file = environ.get_density_param_file(jobname, calcname)
    return cls._from_json(json.loads(open(param_file, 'r').read()))

  def save(self, jobname: str, calcname: str) -> None:
    param_file = environ.get_density_param_file(jobname, calcname)
    with open(param_file, 'w') as f:
      json.dump(self._to_json(), f, indent=2)


class DensityField(object):
  points: np.ndarray
  times: np.ndarray
  densities: np.ndarray # Indexed [time][position]
  normalization: float

  def __init__(
      self,
      sigma2: float,
      points: np.ndarray,
      densities: np.ndarray,
      normalization: float,
      times: np.ndarray) -> None:
    self.sigma2 = sigma2
    self.n_points = len(points)
    self.points = points
    self.densities = densities
    self.normalization = normalization
    self.times = times

  @classmethod
  def load(cls, jobname: str, calcname: str, trial_index: int) -> DensityField:
    return cls.from_file(environ.get_density_file(jobname, calcname, trial_index))

  @classmethod
  def from_file(cls, fname: str) -> DensityField:
    obj = np.load(fname)
    return cls(
      obj['sigma2'],
      obj['points'],
      obj['densities'],
      obj['normalization'],
      obj['times'])

  def save(self, jobname: str, calcname: str, trial_index: int) -> None:
    self.to_file(environ.get_density_file(jobname, calcname, trial_index))

  def to_file(self, fname: str) -> None:
    np.savez(fname, sigma2=self.sigma2, n_points=self.n_points, points=self.points, densities=self.densities, times=self.times, normalization=self.normalization)

  @classmethod
  def generate(
    cls,
    params: Parameters,
    positions: np.ndarray,
    n_points: int,
    sigma2: float,
    times: np.ndarray,
    verbose: bool=True) -> DensityField:
    if params.dims != 1:
      raise Exception('Not implemented yet')

    # Normalization = \frac{1}{\rho_0\sqrt{2\pi\sigma^2}}
    norm = 1/(np.sqrt(2*np.pi*sigma2))
    points = np.linspace(0, params.size, n_points)
    inv_2sigma2 = -1.0/(2 * sigma2)

    densities = np.zeros((params.n_steps, n_points))
    for t in range(params.n_steps):
      if verbose:
        print_progress_bar(t, params.n_steps)

      density = np.zeros((n_points))
      # To take into account periodic boundary conditions we create 2 virtual particles for each particle located at x - L and x + L
      for i in [-1, 0, 1]:
        distances = (positions[t,None,:,0] + i*params.size) - points[:,None]
        distances = norm * np.exp((distances[:,:] * distances[:,:]) * inv_2sigma2)
        density[:] += np.sum(distances, axis=1)
      densities[t,:] = density[:]

    if verbose:
      print_progress_bar(params.n_steps, params.n_steps)
    return DensityField(sigma2, points, densities, norm, times)


