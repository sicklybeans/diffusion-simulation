import numpy as np
from parameters import Parameters, Output

def simulate(params: Parameters) -> Output:
  out = Output(params)

  # Initialize the positions of the particles
  n_p = params.n_particles
  n_d = params.dims
  size = params.size

  positions = size * np.random.random((n_p, n_d))
  n_step = 0

  while n_step < params.n_steps:
    out.write(positions, n_step)

    velocities = params.sqrt2Ddt * np.random.normal(size=(n_p, n_d))

    # Periodic boundary conditions
    positions[:,:] = (positions[:,:] + velocities[:,:]) % size

    n_step += 1
  return out

