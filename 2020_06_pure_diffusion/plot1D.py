from parameters import Parameters, Output
from sim import simulate
import numpy as np
import matplotlib.pyplot as plt
from density import DensityField
from matplotlib.animation import FuncAnimation

def simple_example():
  N = 5

  params = Parameters(1, 100.0, N, 10.0, 0.1, 100.0)
  output = simulate(params)

  positions = output.positions

  fig, ax1 = plt.subplots(1, 1)

  for i in range(N):
    ax1.plot(output.times, positions[:,i,0])

  plt.show()

def racehorse_style():
  """
  This demo demonstrates that the Gaussian density modeling is working correctly
  """
  N = 100
  params = Parameters(1, 100.0, N, 10.0, 0.1, 100.0, 1)
  output = simulate(params)
  positions = output.positions
  dfield = DensityField.generate(params, output.positions, 100, 1.0, output.times)

  fig, (ax1, ax2) = plt.subplots(1, 2)

  ax1.set_ylim(0, N)
  ax1.set_xlim(0, params.size)
  for i in range(N):
    ax1.plot([0, params.size], [i+0.5, i+0.5], linewidth=0.5)
  lines = [ax1.plot([positions[0,i,0]], [i+0.5], marker='.')[0] for i in range(N)]

  ax2.set_ylim(0, 3 * params.density)
  ax2.set_xlim(0, params.size)
  lines.append(ax2.plot(dfield.points, dfield.densities[0])[0])

  print(dfield.normalization)

  def update(frame):
    for i in range(N):
      lines[i].set_xdata([positions[frame,i,0]])
    lines[-1].set_ydata(dfield.densities[frame,:])
    return lines

  ani = FuncAnimation(fig, update, frames=len(positions), blit=True)
  plt.show()

if __name__ == '__main__':
  racehorse_style()
