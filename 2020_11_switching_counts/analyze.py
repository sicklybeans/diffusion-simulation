import numpy as np
import matplotlib.pyplot as plt
import os
import progressbar
from switch_sim import BASE, Output


def get_fit_func(n, times, k_on, k_off):
  if n == 0:
    return times, np.exp(-k_off*times)
  elif n == 1:
    if k_on == k_off:
      return times, k_off * times * np.exp(-k_off * times)
    return times, k_off/(k_on-k_off) * (np.exp(-k_off*times)-np.exp(-k_on*times))
  elif n == 2:
    if k_on == k_off:
      return times, 0.5 * np.exp(-k_on * times) * k_on * k_on * times * times
    kon, koff = k_on, k_off
    factor = koff * kon / ((koff-kon)**2)
    return times, factor * np.exp(-(koff+kon)*times) * (np.exp(koff*times) + np.exp(kon*times) * ((kon-koff)*times - 1))
  else:
    return times, [0 for _ in range(len(times))]

COLORS = ['red', 'orange', 'green', 'blue', 'purple']

def analyze_n_flips_side_by_side():
  base_base_dir = '%s/compute_n_flips' % BASE
  fig, (ax1) = plt.subplots(1, 1, figsize=(6.0, 6.0))

  for d in progressbar.progressbar(list(os.listdir(base_base_dir))):
    base_dir = '%s/%s' % (base_base_dir, d)
    base_out_dir = '%s' % base_dir

    for file in os.listdir(base_dir):
      if file.endswith('_output.npz'):
        output = Output.load_file('%s/%s' % (base_dir, file))
        out_dir = '%s/kOn=%.2f_kOff=%.2f' % (base_out_dir, output.params.k_on, output.params.k_off)

        plt.cla()
        ax1.set_xlabel('time')
        ax1.set_ylabel('Fraction of (n) flips')
        ax1.set_ylim(0, 1.1)
        ax1.set_title('N Flips for k_on=%.2f, k_off=%.2f, a=%.2f' % (output.params.k_on, output.params.k_off, output.params.compute_peq()))
        totals = np.zeros(output.params.num_times)

        for i in [0,1,2,3,4]:
          k_on = output.params.k_on
          k_off = output.params.k_off
          times = output.params.times
          tvals, predics = get_fit_func(i, times, k_on, k_off)

          frac = output.num_n_flips[i] / output.params.n
          totals = totals + frac

          ax1.plot(times, frac, label='%d flips (actual)' % i, color=COLORS[i])
          ax1.plot(tvals, predics, color=COLORS[i], label='%d flips (predicted)' % i, linestyle='dashed', linewidth=2)

        ax1.plot(times, totals, label='Summed flips', color='black')
        ax1.legend()
        plt.savefig('%s_n-flips.png' % (out_dir))


if __name__ == '__main__':
  analyze_n_flips_side_by_side()
