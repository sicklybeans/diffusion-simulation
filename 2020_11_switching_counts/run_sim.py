import numpy as np
import os
import itertools
import switch_sim
import progressbar

def compute_n_flips():
  n = 100000
  t_stop = 20.0
  t_step = 0.1

  base_dir_f = lambda x,y : 'compute_n_flips/kOn=%.2f/kOff=%.2f' % (x, y)
  k_ons = np.linspace(0, 2, 11)[1:]
  k_offs = np.linspace(0, 2, 11)[1:]
  vals = list(itertools.product(k_ons, k_offs))
  for k_on, k_off in progressbar.progressbar(vals):
    base_name = base_dir_f(k_on, k_off)
    p = switch_sim.Parameters(n, t_step, t_stop, k_on, k_off)
    switch_sim.run(p, base_name, verbose=False)

if __name__ == '__main__':
  compute_n_flips()
