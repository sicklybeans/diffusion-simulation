from __future__ import annotations
import numpy as np
import os
import sys
import random
import progressbar

BASE = '/run/media/sam/scratch60/poisson_switching_sim'
if not os.path.exists(BASE):
  os.makedirs(BASE)

class Parameters(object):
  n: int
  dt: float
  t_end: float
  k_on: float
  k_off: float
  num_times: np.ndarray

  def __init__(self, n: int, dt: float, t_end: float, k_on: float, k_off: float) -> None:
    self.n = n
    self.dt = dt
    self.t_end = t_end
    self.num_times = int(np.ceil(t_end/dt))
    self.times = np.array([self.dt * i for i in range(self.num_times)])
    self.k_on = k_on
    self.k_off = k_off

  def compute_peq(self) -> float:
    return self.k_on/(self.k_on + self.k_off)

  def save(self, base_name: str) -> None:
    os.makedirs(os.path.dirname(base_name), exist_ok=True)
    np.savez('%s/%s_params.npz' % (BASE, base_name),
      n=self.n,
      dt=self.dt,
      t_end=self.t_end,
      k_on=self.k_on,
      k_off=self.k_off)

  @classmethod
  def load(cls, base_name: str) -> Parameters:
    params_d = np.load('%s_params.npz' % (base_name))
    return cls(**params_d)

class Output():
  params: Parameters
  num_flips_stored: int
  fraction: np.ndarray
  num_n_flips: np.ndarray
  _ready: bool

  def __init__(self, params: Parameters, num_flips_stored: int=5):
    self.params = params
    self.num_flips_stored = num_flips_stored
    self._ready = False

  def run(self, verbose: bool=True) -> None:
    self._prepare()
    bar = verbose and progressbar.ProgressBar() or progressbar.NullBar()

    # Initialize species state using random assignment weighted by equilibrium value
    states = np.array([True for _ in range(self.params.n)], dtype=bool)
    flips = np.zeros(self.params.n, dtype=int)
    k_on, k_off = self.params.k_on, self.params.k_off
    prob_on  = k_on * self.params.dt
    prob_off = k_off * self.params.dt

    for step in bar(range(self.params.num_times)):
      self._write(step, states, flips)
      rng = np.random.random(self.params.n)

      # Fast method
      flips[:] += (states[:] * (rng < prob_off)) + (~states[:] * (rng < prob_on))
      states = (states * (rng > prob_off)) + (~states * (rng < prob_on))

      # # Slow safe method
      # for i in range(self.params.n):
      #   if states[i]:
      #     if rng[i] < prob_off:
      #       flips[i] += 1
      #       states[i] = False
      #   else:
      #     if rng[i] < prob_on:
      #       flips[i] += 1
      #       states[i] = True

    self._ready = False

  @classmethod
  def basename_to_fname(cls, base_name: str) -> str:
    return '%s/%s_output.npz' % (BASE, base_name)

  def _prepare(self) -> None:
    self.fraction = np.zeros(self.params.num_times)
    self.num_n_flips = np.zeros((self.num_flips_stored, self.params.num_times))
    self._ready = True

  def _write(self, step: int, states: np.ndarray, nflips: np.ndarray) -> None:
    if not self._ready:
      raise Exception('Output.prepare must be called before writing data')
    self.fraction[step] = np.sum(states) / self.params.n
    for i in range(self.num_flips_stored):
      self.num_n_flips[i,step] = np.sum(nflips == i)

  def save(self, base_name: str) -> None:
    os.makedirs(os.path.dirname(base_name), exist_ok=True)
    self.params.save(base_name)
    np.savez(Output.basename_to_fname(base_name),
      num_n_flips=self.num_n_flips,
      num_flips_stored=self.num_flips_stored,
      # nflips=self.nflips,
      fraction=self.fraction)

  @classmethod
  def load(cls, base_name: str) -> Output:
    return Output.load_file(Output.basename_to_fname(base_name))

  @classmethod
  def load_file(cls, file: str) -> Output:
    if file.endswith('_output.npz'):
      base_name = file[0:-11]
    else:
      raise Exception('Unexpected filename: %s' % file)
    params = Parameters.load(base_name)
    outputd = np.load(file)
    output = Output(params, num_flips_stored=outputd['num_flips_stored'])
    output.num_n_flips = outputd['num_n_flips']
    output.fraction = outputd['fraction']
    return output

def run(params: Parameters, base_name: str, skip_existing: bool=True, verbose: bool=True) -> None:
  fname = Output.basename_to_fname(base_name)
  if os.path.exists(fname):
    if skip_existing:
      if verbose:
        print('Ignoring existing file %s' % fname)
      return
    else:
      os.remove(fname)
  if not os.path.exists(os.path.dirname(fname)):
    os.makedirs(os.path.dirname(fname))

  output = Output(params)
  output.run(verbose=verbose)
  output.save(base_name)
  if verbose:
    print('Saved output %s' % fname)

# def compute_reaction_diffusion():
#   n = 100000
#   t_stop = 1.0
#   t_step = 0.001
#   base_dir = 'test10'

#   def compute_reaction_diffusion_vary_k_on():
#     k_ons = np.linspace(0, 2, 51)[1:]
#     k_off = 1.0
#     for k_on in progressbar.progressbar(k_ons):
#       run(Parameters(n, t_step, t_stop, 1.0, 1.0, k_on, k_off), '%s/k_on/run_%.2f' % (base_dir, k_on), verbose=False)

#   def compute_reaction_diffusion_vary_k_off():
#     k_offs = np.linspace(0, 2, 51)[1:]
#     k_on = 1.0
#     for k_off in progressbar.progressbar(k_offs):
#       run(Parameters(n, t_step, t_stop, 1.0, 1.0, k_on, k_off), '%s/k_off/run_%.2f' % (base_dir, k_off), verbose=False)
#     # util.print_progress_bar(10, 10)

#   def compute_reaction_diffusion_vary_both():
#     ks = np.linspace(0, 2, 51)[1:]
#     for k in progressbar.progressbar(ks):
#       run(Parameters(n, t_step, t_stop, 1.0, 1.0, k, k), '%s/k_both/run_%.2f' % (base_dir, k), verbose=False)
#   print('Computing for varied k_on set..')
#   compute_reaction_diffusion_vary_k_on()
#   print('Computing for varied k_off set...')
#   compute_reaction_diffusion_vary_k_off()
#   print('Computing for varied k_on/k_off set...')
#   compute_reaction_diffusion_vary_both()

# def final_reaction_correlation_test():
#   n = 100000
#   t_stop = 3.0
#   t_step = 0.001
#   base_dir = 'test11'
#   num_runs = 20

#   for i in progressbar.progressbar(range(num_runs)):
#     peq = random.random()
#     k_on = random.random()
#     k_off = k_on/peq - k_on
#     run(Parameters(n, t_step, t_stop, 1.0, 1.0, k_on, k_off), '%s/run_peq%.2f_kon%.2f' % (base_dir, peq, k_on), verbose=False)

# if __name__ == '__main__':

  # final_reaction_correlation_test()
  # compute_reaction_diffusion()
  # compute_reaction_diffusion_vary_k_on()
  # compute_reaction_diffusion_vary_k_off()
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.2), 'test3/run_0.2')
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.4), 'test3/run_0.4')
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.6), 'test3/run_0.6')
  # run(Parameters.from_peq(500, 0.05, 1000.0, 1.0, 1.0, 0.8), 'test3/run_0.8')
