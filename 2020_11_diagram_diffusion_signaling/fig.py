import numpy as np
import matplotlib.pyplot as plt
from typing import Tuple
from matplotlib.animation import FuncAnimation
from generate_bounded import *
from matplotlib.patches import Circle, Rectangle
import matplotlib.patches as patches
# from parameters import Parameters, Output
# from sim import simulate
# from density import DensityField

OFF_COLOR = '#D9CDBD'
BG_COLOR = '#c7d2d9'
ACT_COLOR = '#C22924'
SEN_COLOR = '#4D8ED3'
REC_COLOR = '#F2D64D'
OUTLINE_COLOR = 'black'
OUTLINE_WIDTH = 1.1
DOT_SIZE = 0.19
DOT_SPACING = 0.8
XY_FONT_SIZE = 30
INCLUDE_INACTIVE = True

def quadratic(dr, slope, base):
  x = np.sqrt(np.dot(dr, dr))
  return base - slope * x * x

def exponential(dr, rate):
  x = np.sqrt(np.dot(dr, dr))
  return np.exp(- rate * x)

def linear(dr, slope, base):
  return base - (np.sqrt(np.dot(dr, dr)) * slope)

def gaussian(dr, sigma, base):
  return base * np.exp(-np.dot(dr, dr)/(2 * sigma * sigma))

# def draw_speech_bubbles(ax):
#   # ax.add_artist(Circle((35, 35), 10, color='white'))
#   ax.add_artist(patches.Ellipse((40, 40), 20, 10, color='white'))
#   ax.text(40, 40, 'X', fontsize=24,
#     verticalalignment='center', horizontalalignment='center')
#   # ax.add_artist(Rectangle(35, 35), 30, 30, color='white', )

def draw_circle_roy_lichenstein(ax, pos, radius, color):
  ax.add_artist(plt.Circle(pos, radius, color='white', linewidth=OUTLINE_WIDTH))

  n = int(np.ceil((2 * radius) / DOT_SPACING) + 1)
  offset = (2 * radius - DOT_SPACING * n) / 2
  for xi in range(n):
    for yi in range(n):
      y = pos[1] - radius + DOT_SPACING * yi + offset
      x = pos[0] - radius + DOT_SPACING * xi + offset + (DOT_SPACING / 2) * (yi % 2)
      if (((x - pos[0]) ** 2) + ((y - pos[1]) ** 2)) <= radius * radius:
        ax.add_artist(plt.Circle((x, y), DOT_SIZE, color=color))
  ax.add_artist(plt.Circle(pos, radius, fill=False, edgecolor=OUTLINE_COLOR, linewidth=OUTLINE_WIDTH))

def draw_rect_roy_lichenstein(ax, pos, width, height, color):
  x = pos[0]
  while x < pos[0] + width:
    y = pos[1]
    i = 0
    while y < pos[1] + height:
      ax.add_artist(Circle((x + (i % 2) * (DOT_SPACING / 2), y), DOT_SIZE, color=color))
      y += DOT_SPACING
      i += 1
    x += DOT_SPACING

def go(size: Tuple[float, float], num, radius, sender_pos, rec_pos, sender_radius, sigma, profile: str):
  aspect = size[1] / size[0];
  fig, ax = plt.subplots(1, 1, figsize=(6, aspect * 6))
  ax.set_xlim(0, size[0])
  ax.set_ylim(0, size[1])

  # draw_rect_roy_lichenstein(ax, (0, 0), size[0], size[1], BG_COLOR)
  ax.add_artist(Rectangle((0, 0), size[0], size[1], color=BG_COLOR))

  positions = generate_sized_particles(num, radius, [(0, size[0]), (0, size[1])])
  # positions = np.random.random((num, 2)) * size

  circles = []
  for i, pos in enumerate(positions):
    dr = pos - sender_pos
    if profile == 'linear':
      chance = linear(dr, 0.01, 0.65)
    elif profile == 'quadratic':
      chance = quadratic(dr, 0.0005, 0.65)
    elif profile == 'exponential':
      chance = exponential(dr, 0.03)
    else:
      chance = gaussian(dr, sigma, 0.65)
    if np.random.random() < chance:
      circles.append(ax.add_artist(plt.Circle(positions[i], radius, facecolor=ACT_COLOR, edgecolor=OUTLINE_COLOR, linewidth=OUTLINE_WIDTH)))
    else:
      if INCLUDE_INACTIVE:
        circles.append(ax.add_artist(plt.Circle(positions[i], radius, facecolor=OFF_COLOR, edgecolor=OUTLINE_COLOR, linewidth=OUTLINE_WIDTH)))
  # draw_circle_roy_lichenstein(ax, sender_pos, sender_radius, SEN_COLOR)

  ax.add_artist(plt.Circle(sender_pos, sender_radius, facecolor=SEN_COLOR, edgecolor=OUTLINE_COLOR, linewidth=2.0))
  ax.add_artist(plt.Circle(rec_pos, sender_radius, facecolor=REC_COLOR, edgecolor=OUTLINE_COLOR, linewidth=2.0))
  ax.text(sender_pos[0], sender_pos[1], 'X', fontsize=XY_FONT_SIZE, fontweight='bold',
    verticalalignment='center', horizontalalignment='center')
  ax.text(*rec_pos, 'Y', fontsize=XY_FONT_SIZE, fontweight='bold',
    verticalalignment='center', horizontalalignment='center')
  # draw_speech_bubbles(ax)


  draw_distance_label(ax, sender_pos, rec_pos, sender_radius, sender_radius)
  remove_circles(ax, circles, sender_pos, rec_pos)

  plt.axis('off')
  plt.tight_layout()
  plt.subplots_adjust(left=0, bottom=0, right=1, top=1)
  fig.savefig('diffusion-2D.pdf')
  plt.show()

def draw_arrow(ax, x1, x2, y1, y2, c='black', lw=1.5, arrow_length=5):
  th = np.arctan2(y1 - y2, x1 - x2)
  th1 = th + 3.14159/8
  th2 = th - 3.14159/8
  x3 = x2 + arrow_length * np.cos(th1)
  y3 = y2 + arrow_length * np.sin(th1)
  x4 = x2 + arrow_length * np.cos(th2)
  y4 = y2 + arrow_length * np.sin(th2)
  ax.plot([x1, x2], [y1, y2], color=c, linewidth=lw)
  ax.plot([x2, x3], [y2, y3], color=c, linewidth=lw)
  ax.plot([x2, x4], [y2, y4], color=c, linewidth=lw)

def draw_distance_label(ax, s_pos, r_pos, s_radius, r_radius):

  x1 = s_pos[0] + s_radius * np.cos(3.1415/4) + 2
  y1 = s_pos[1] + s_radius * np.sin(3.1415/4) + 2
  x3 = r_pos[0] - r_radius * np.cos(3.1415/4) - 2
  y3 = r_pos[1] - r_radius * np.sin(3.1415/4) - 2
  x2 = (x1 + x3) / 2
  y2 = (y1 + y3) / 2
  offset = 3
  draw_arrow(ax, x2 - offset, x1, y2 - offset, y1, arrow_length=3)
  draw_arrow(ax, x2 + offset, x3, y2 + offset, y3, arrow_length=3)
  ax.text(x2, y2, r'$r$', fontsize=20, ha='center', va='center')

def remove_circles(ax, circles, s_pos, r_pos):
  m = (r_pos[1] - s_pos[1])/(r_pos[0] - s_pos[0])
  m_invm = 1/m + m

  x1, y1 = s_pos[0], s_pos[1]
  x2, y2 = r_pos[0], r_pos[1]
  for c in circles:
    xc, yc = c.center[0], c.center[1]
    if xc > x1 and xc < x2 and yc > y1 and yc < y2:
      # Compute distance between circle center and line.
      x = (xc/m + m * x1 + yc - y1)/m_invm
      y = m * (x - x1) + y1
      distance = np.sqrt((x - xc)**2 + (y - yc)**2)
      if distance < 4:
        c.remove()

if __name__ == '__main__':
  size = (100, 100)
  num = 700
  rad = 1.0
  sender_pos = np.array([25, 25])
  rec_pos = np.array([75, 75])
  sender_rad = 10.0
  sigma = 30
  go(size, num, rad, sender_pos, rec_pos, sender_rad, sigma, 'exponential')
